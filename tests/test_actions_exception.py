import pydinemic
import time

create = False
created = False
update = False
updated = False

class Listener(pydinemic.DAction):
    create = False
    created = False
    update = False
    updated = False
    
    def on_create(self, object_id, key):
        print("create")
        self.create = "create"
        
    def on_created(self, object_id, key):
        print("created")
        self.created = "created"
    
    def on_update(self, object_id, key, old_value, new_value):
        self.update = "update"
        raise Exception('blocked')
        
    def on_updated(self, object_id, key, old_value, new_value):
        self.updated = "updated"
    

class TestModelExceptions(pydinemic.DModel):
    pass

    
def test_action():
    pydinemic.launch()
    
    listener = Listener()
    listener.apply('TestModelExceptions:[id]')
    listener.apply('TestModelExceptions:[id]:*')
    
    assert(listener.create == False)
    assert(listener.created == False)
    assert(listener.update == False)
    assert(listener.updated == False)
    
    d = TestModelExceptions([])
    
    assert(listener.create == "create")
    assert(listener.created == "created")
    
    # Authorized keys update sets it to new values
    assert(listener.update == "update")
    
    # on_update is not permiting execution of on_updated due to exception taised
    assert(listener.updated == False)
    
    listener.update = False
    listener.updated = False
      
    d.set('a', '2')
    
    assert(listener.update == "update")
    assert(listener.updated == False)
  
