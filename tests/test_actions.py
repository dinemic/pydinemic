import pydinemic
import time

create = False
created = False
update = False
updated = False

class Listener(pydinemic.DAction):
    create = False
    created = False
    update = False
    updated = False
    
    def on_create(self, object_id, key):
        self.create = "create"
        
    def on_created(self, object_id, key):
        self.created = "created"
    
    def on_update(self, object_id, key, old_value, new_value):
        print('xxx')
        self.update = "update"
    
    def on_updated(self, object_id, key, old_value, new_value):
        self.updated = "updated"


class TestModelListener(pydinemic.DModel):
    pass

    
def test_action():
    pydinemic.launch()
    
    listener = Listener()
    listener.apply('TestModelListener:[id]')
    listener.apply('TestModelListener:[id]:*')
    
    assert(listener.create == False)
    assert(listener.created == False)
    assert(listener.update == False)
    assert(listener.updated == False)
    
    d = TestModelListener([])
    
    assert(listener.create == "create")
    assert(listener.created == "created")
    
    # Authorized keys update sets it to the true
    assert(listener.update == "update")
    assert(listener.updated == "updated")
    
    listener.update = False
    listener.updated = False
      
    d.set('a', '2')
    
    assert(listener.update == "update")
    assert(listener.updated == "updated")
  
