import pydinemic
import time


def test_dfield():
    pydinemic.launch()
    
    class TestModelDField(pydinemic.DModel):
        f = pydinemic.DField('TestFieldF', False)
        g = pydinemic.DField('TestFieldG', True)

    d = TestModelDField([])
    d.f.set('a')
    d.g.set('b')

    time.sleep(1)

    print(d.get('TestFieldG', 'none'))

    assert(d.get('TestFieldF', 'none') == 'a')
    assert(d.get('TestFieldG', 'none') != 'none')
    assert(d.get('TestFieldG', 'none') != 'b')
    assert(d.get('TestFieldG', 'none') != '')
    assert(d.f.get() == 'a')
    assert(d.g.get() == 'b')
