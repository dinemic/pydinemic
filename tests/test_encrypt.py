import pydinemic

def test_encrypt():
    pydinemic.launch()
    pydinemic.set_loglevel('error')
    
    class TestModelEncrypt(pydinemic.DModel):
        pass
      
    c = TestModelEncrypt([])
    
    encrypted = c.encrypt('abcd')
    decrypted = c.decrypt(encrypted)
    
    assert(decrypted == 'abcd')
    assert(encrypted != 'abcd')

