import pydinemic

def test_dmodel():
    pydinemic.launch()
    pydinemic.set_loglevel('error')
    
    class TestModel(pydinemic.DModel):
        pass
      
    c = TestModel([])
    c.set('a', '2')

    assert(c.get('a', 'none') == '2')

